var express = require('express');
var sha256 = require('sha256');
var router = express.Router();
var tablaUsers = require("../models/users");
var rest = require("../mymodules/rest");
var hbsmailer = require("../mymodules/hbsmailer");

/* GET home page. */
router.get('/', function(req, res, next) {



});




router.get('/hey', function(req, res, next) {
  if(req.session.user){
    res.json({"message":"Hey dude!", "user":req.session.user})    
  }
  else{
    res.json({"message":"I'm sorry,... who are you???"})        
  }
});

router.post('/login', function(req, res, next) {	
	tablaUsers.findOne({
	  where: {username: req.body.username, password: sha256(req.body.password)}
	}).then(user => {
		if(user){
			req.session.user = user;
			res.json({"message":"Welcome "+user.name+"!"});			
		}else{
			res.json({"message":"Nah Nah Nah!"});					
		}
	}).catch(function(error){
		console.log("Whoops!, Something happened...");		
		console.log(error);		
	});	
});

router.get('/facebook_redirect', function(req, res, next) {
	//capturar datos de facebook 	
	var redirect_uri = "http://mydailyevent.com:3000/facebook_redirect";
	var facebook_code = req.query.code;
	var accesstoken_call = {
	    host: 'graph.facebook.com',
	    port: 443,
	    path: '/v2.10/oauth/access_token?client_id=354793484967340&redirect_uri='+redirect_uri+
	    '&client_secret=93a805f49354101dc30a7248142154e5&code='+facebook_code,
	    method: 'GET',
	    headers: {
	        'Content-Type': 'application/json'
	    }
	};
	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {	    
	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
	    console.log("token: " + access_token_response.access_token);
	    console.log("FB_path: " + FB_path);
	    var userinfo_call = {
		    host: 'graph.facebook.com',
		    port: 443,
		    path: FB_path,
		    method: 'GET',
		    headers: {
		        'Content-Type': 'application/json'
		    }
		};
		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {	    	
	    	console.log(user_info_response);
			tablaUsers.find( { query : { "facebookID" : user_info_response.id } } ).then(function(user){ //facebookID en tabla de usuarios y facebook access token
				user.facebook_access_token = access_token_response.access_token;
				user.save();
			});
			res.redirect("http://mydailyevent.com/index.html");  
		});
	});
});

router.post('/loginFB', function(req, res, next) {
	var redirect_uri = "http://mydailyevent.com:3000/facebook_redirect";
	res.redirect("https://www.facebook.com/v2.10/dialog/oauth?scope=email,public_profile&client_id=354793484967340&redirect_uri="+redirect_uri);  
});

module.exports = router;
