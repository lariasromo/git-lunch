var sha256 = require('sha256');
var express = require('express');
var hbsmailer = require("../mymodules/hbsmailer");
var router = express.Router();

var tablaUsers = require('../models/users');

router.get('/', function(req, res, next) {
  tablaUsers.findAll().then(function(users) {
    res.json(users);
  });
});




router.post('/create', function(req, res, next) {
  var current_time = new Date().getTime();
  var newUserData = {
      "email":req.body.email, 
      "name":req.body.name, 
      "username":req.body.username, 
      "password":sha256(req.body.password),
      "address":req.body.address, 
      "status":"active" 
  };
  console.log("Creando usuario...");
  console.log(newUserData);

    tablaUsers.build(newUserData).save().then(function(new_user){
        //SUCCESS!! :)
      console.log("Usuario creado...");
      console.log(new_user);
      hbsmailer.sendMail({
           from: 'luiantonii@gmail.com',
           to: req.body.email,
           subject: 'Welcome ' + req.body.name,
           template: 'welcome',
           context: {
                name : req.body.name,
                confirm_link : 'http://localhost:3000/users/confirm_email/' + new_user.userid
           }
       }, function (error, response) {
           console.log(error);
           console.log(response);
           console.log('mail sent');
           hbsmailer.close();
          res.send("hello");
       });

    createJson = {"message":"Se ha creado el usuario con exito!", "created":true };          
        res.json(createJson);
    }).catch(function (err) {
        //ERROR!! :(
        console.log(err);
		createJson = {"message":"Ha ocurrido un error :(", "created":false };  				 
        res.json(createJson);
    });


});


router.post('/login', function(req, res, next) {
  var responselogin = {};
  console.log(req.body);

  var query = {"email":req.body.email, "password":sha256(req.body.password) };

  tablaUsers.findOne( { where: query } ).then(function(user)
	{
		if(user){
			responselogin = {"message":"Login exitoso", "login":true };  				 
		}else{
			responselogin = {"message":"Login fallido", "login":false };  	  	
		}
  		res.json(responselogin);
	});


});

router.get('/whoisdis',function(req, res, next){
  if (req.session.user){
    res.json({"message": "you are here", "username":req.session.user.name});
  }
  else{
    res.json({"message":"you are dead"});
  }
});
/*
router.delete('/User/:id', function(req, res, next){///CRISTIANO AAAAAAAAAAAAAAAA!!!!!!!
  var id =req.params.id;
    tablaUsers.destroy({

    })
});

router.get('/secret'

  );
*/
module.exports = router;
