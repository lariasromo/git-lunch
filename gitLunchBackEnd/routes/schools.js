var express = require('express');
var multer  = require('multer');
var fs  = require('fs');

var upload = multer({dest: '/app/pictures/'});
var type = upload.single('profilePicture');

var router = express.Router();

var institution_table = require('../models/schools');

/* GET home page. */
router.get('/', function(req, res, next) {
  institution_table.findAll().then(function(institution) {
    res.json(institution);
  });
});

router.get('/profileimage/:id', function(req, res, next) {
  var idSchool = req.params.id;
  institution_table.findOne({
    where: { idSchool: idSchool }
  }).then(school => {
    if(school){
      var img = fs.readFileSync("/app/pictures/"+school.profilePicture);
      res.writeHead(200, { 'Content-Type': 'image/jpeg' });
      res.end(img, 'binary');
    }
  }).catch(function(error){
    console.log("Whoops!, Something happened...");    
    console.log(error);   
  }); 
});

router.post('/uploadPicture/:id', type, function(req, res, next) {
  var idSchool = req.params.id;
  institution_table.findOne({
    where: { idSchool: idSchool }
  }).then(school => {
    if(school){
      school.profilePicture = req.file.filename;
      school.save();
      console.log(req.file);
      console.log(req.files);
      if (!req.files) {
        res.send('No files were uploaded.');
        return;
      }
      
    }else{      
    }
  }).catch(function(error){
    console.log("Whoops!, Something happened...");    
    console.log(error);   
  }); 
});

router.post('/testUpload', type, function(req, res, next) {    
    try{
      console.log(req.file);
      
    }catch (e) {
      console.error(e.stack);
      console.log("error");
    }
});



router.post('/create', function(req, res, next) {
  var current_time = new Date().getTime();
  var newInstitucion= {
      "school":req.body.school, 
      "address":req.body.address, 
      "phone":req.body.phone, 
      "location":req.body.location,
      "email":req.body.email, 
      "description":req.body.description, 
      "profilePicture":"", 
  };
  console.log("Creando nueva escuela...");
  console.log(newInstitucion);

    institution_table.build(newInstitucion).save().then(function(){
        //SUCCESS!! :)
    createJson = {"message":"Se ha agregado una nueva escuela!", "created":true };          
        res.json(createJson);
    }).catch(function (err) {
        //ERROR!! :(
        console.log(err);
		createJson = {"message":"Ha ocurrido un error.", "created":false };  				 
        res.json(createJson);
    });


});

module.exports = router;
