var express = require('express');
var sha256 = require('sha256');
var router = express.Router();
var paypal = require('paypal-rest-sdk');

var tablaPayment = require("../models/payment");

paypal.configure({
  'mode': 'sandbox', //sandbox or live
  'client_id': 'AdpKyeCACPirFCO59LTAVTEniNK1v1MFONQRuMsygsuhYG7jQAzkOTGGSuSEwgNa8Cqaw3NNaD_A7Kr1',
  'client_secret': 'EK_1sjTGHqgrm-nNw-ljAy3YcI-etIacF084BOYL69hdbRmT1_hOOZ2OXJ_NM3qt5cW-xd-CCriBvfa4'
});

/* GET home page. */
router.post('/paypal_create', function(req, res, next) {
	console.log(req.body);

	var create_payment_json = {
	    "intent": "sale",
	    "payer": {
	        "payment_method": "paypal"
	    },
	    "redirect_urls": {
	        "return_url": "http://localhost/paypal/success",
	        "cancel_url": "http://localhost/paypal/error"
	    },
	    "transactions": [{	       
	        "amount": {
	            "currency": "MXN",
	            "total": req.body.qty
	        },
	        "description": "This is the payment description."
	    }]
	};

	paypal.payment.create(create_payment_json, function (error, payment) {
	    if (error) {
		  	console.log(error);	        
	    } else {
	        console.log("Create Payment Response");
	        console.log(payment);
			var paymentData = {
				paypalCreate : create_payment_json,
				paypalCreateResponse : payment,
				payPalPaymentID : payment.id,
				qty : req.body.qty,
				status : "created"
			};
			tablaPayment.build(paymentData).save().then(function(){		
	        	res.redirect(payment.links[1].href)
			}).catch(function(err){
			  	console.log(err);
		    });
	    }
	});


});


router.get('/error', function(req, res, next) { 
	var paymentData = {
		status : "failed"
	};

	tablaPayment.update(paymentData, {
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){

		res.send("Paypal Error");
    });
});

router.get('/success', function(req, res, next) { 
	var paymentData = {
		payerId : req.query.PayerID,
		paypalToken : req.query.token,
		status : "completed"
	};

	tablaPayment.update(paymentData, {
        where: {
            payPalPaymentID : req.query.paymentId
        }
    }).then(function(){

		res.send("Paypal Success");
    });


});

module.exports = router;