var sequelize = require("../mymodules/rawSQL");
var express = require('express');
var router = express.Router();

var tablaLightUsers = require('../models/lightUser');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/login', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get("/gender",function(req,res,next){
	// [
 //      ['Gender', 'Qty'],
 //      ['Male',    50],
 //      ['Female',  150]
 //    ]
 	var result = [ ['Gender', 'Qty'] ];
 	var genderQuery = "select gender, count(1) as qty from lightUsers group by gender";
 	sequelize.query(genderQuery, { type: sequelize.QueryTypes.SELECT})
	.then(rows => {	    
		console.log(rows);
		for(rowIndex in rows){
			row = rows[rowIndex];
			result.push([row.gender, row.qty]);
		}
		res.json(result);
	});
});

router.get("/ages",function(req,res,next){

});

module.exports = router;
