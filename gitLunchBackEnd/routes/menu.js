var express = require('express');
var router = express.Router();

var tablaProducts = require('../models/products');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/login', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/createproduct', function(req, res, next) {
  var query = {"name":req.body.name,"price":req.body.price, "description":req.body.description, "brand":req.body.brand,"portion":req.body.portion, "ingredients":req.body.ingredients };
  tablaProducts.save(query);
});


module.exports = router;
