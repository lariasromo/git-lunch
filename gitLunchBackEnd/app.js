var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sessions = require('express-session');

var index = require('./routes/index');
var users = require('./routes/users');
var menu = require('./routes/menu');
var products = require('./routes/products');
var schools = require('./routes/schools');
var paypal = require('./routes/paypal');
var analytics = require('./routes/analytics');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

   next();
}

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(allowCrossDomain);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(sessions({
  cookieName: 'session',
  secret: 'secret here, generate something random',
  duration: 24 * 60 * 60 * 1000, // ms
  activeDuration: 1000 * 60 * 5 
}));

app.use('/', index);
app.use('/app', express.static("../gitLunchFront"));
app.use('/users', users);
app.use('/', menu);
app.use('/paypal', paypal);
app.use('/products', products);
app.use('/schools', schools);
app.use('/analytics', analytics);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
