var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'Welcome321';
}

var sequelize = new Sequelize('gitLunch', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});
var institution = sequelize.define('institutions', {
    "idSchool": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },    
    "profilePicture": { type: Sequelize.STRING },    
    "school": { type: Sequelize.STRING},
    "address": { type: Sequelize.STRING},
    "phone": { type: Sequelize.STRING},
    "location" : { type: Sequelize.JSON, allowNull: true},
    "email": { type: Sequelize.STRING, allowNull: true},
    "description": { type: Sequelize.STRING, allowNull: true},
}); 
institution.sync();
module.exports = institution;