var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'Welcome321';
}

var sequelize = new Sequelize('gitLunch', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});


var products = sequelize.define('products', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	"Name"				: { type: Sequelize.STRING },
	"description"				: { type: Sequelize.STRING },
	"price"				: { type: Sequelize.STRING },
	"brand"				: { type: Sequelize.STRING },
	"portion"				: { type: Sequelize.STRING },
	"ingredients"				: { type: Sequelize.STRING }

});

products.sync();
module.exports = products;