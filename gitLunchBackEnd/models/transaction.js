var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'Welcome321';
}

var sequelize = new Sequelize('gitLunch', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});


var transaction = sequelize.define('transaction', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	"place"				: { type: Sequelize.STRING },
	"location"			: { type: Sequelize.JSON },
	"datetime"			: { type: Sequelize.DATE },
	"max_guests"		: { type: Sequelize.INTEGER },
	"current_guests"	: { type: Sequelize.INTEGER },
	"name"				: { type: Sequelize.STRING },
	"admin"				: { type: Sequelize.INTEGER },
	"date_created"		: { type: Sequelize.DATE },
	"description"		: { type: Sequelize.STRING },
	"status"			: { type: Sequelize.STRING },
	"deadline"			: { type: Sequelize.DATE }

});

transaction.sync();
module.exports = event;