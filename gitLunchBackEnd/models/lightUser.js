var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'Welcome321';
}

var sequelize = new Sequelize('gitLunch', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});


var lightUser = sequelize.define('lightUser', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	"name"				: { type: Sequelize.STRING },
	"age"				: { type: Sequelize.STRING },
	"gender"			: { type: Sequelize.STRING }
});

lightUser.sync();
module.exports = lightUser;