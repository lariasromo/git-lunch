var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = 'Welcome321';
}

var sequelize = new Sequelize('gitLunch', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});
var users = sequelize.define('users', {
    "userid": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
	"email" : { type: Sequelize.STRING },
    "name": { type: Sequelize.STRING },
    "username" : { type: Sequelize.STRING },
    "password" : { type: Sequelize.STRING },
    "address" : { type: Sequelize.STRING },
    "city" : { type: Sequelize.INTEGER, allowNull: true },
    "birthday" : { type: Sequelize.DATEONLY, allowNull: true },
    "gender" : { type: Sequelize.CHAR, allowNull: true },
    "school" : { type: Sequelize.STRING, allowNull: true },
    "country" : { type: Sequelize.STRING, allowNull: true },
    "location" : { type: Sequelize.GEOMETRY, allowNull: true },
    "interests" : { type: Sequelize.JSON , allowNull: true},
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
}); 
users.sync();
module.exports = users;