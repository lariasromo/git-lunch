// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var placeSearch, autocomplete;
    
function initAutocomplete() {
// Create the autocomplete object, restricting the search to geographical
// location types.
autocomplete = new google.maps.places.Autocomplete(
    /** @type {!HTMLInputElement} */(document.getElementById('address')),
    {types: ['geocode']});

// When the user selects an address from the dropdown, populate the address
// fields in the form.
autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
// Get the place details from the autocomplete object.
var place = autocomplete.getPlace();
$("#address").val(place.formatted_address);
}


$(document).ready(function(){

	$("#submitbtn").click(function(){
		var email = $("#email").val();
		var password = $("#password").val();
		var name = $("#name").val();
		var username = $("#username").val();
		var address = $("#address").val();


		var jsondata = {
			"email":email, 
			"name":name, 
			"username":username, 
			"address":address, 
			"password":password
		};
		console.log(jsondata);

		$.post("http://localhost:3000/users/create", jsondata, function(data){
			alert(data.message);
			if(data.login){
				window.location = "index.html";
			}
			console.log(data);
		}, "json");
	});


});